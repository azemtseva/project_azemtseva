package entity;

public interface TransactionModel {
    int getId();
    void setId(int id);

    String getName();
    void setName(String name);

    Double getAmount();
    void setAmount(Double Amount);

    Boolean getProfitOrLoss();
    void setProfitOrLoss(Boolean ProfitOrLoss);

    TransactionType getType();
    void setType(TransactionType Type);

    AccountModel getAccount();
    void setAccount(AccountModel Account);
}
