package entity;

public interface ParentCategory {

    int getId();
    void setId(int id);

    String getName();
    void setName(String name);

}
