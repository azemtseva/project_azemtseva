package entity;

public interface TransactionType {

    int getId();
    void setId(int id);

    String getName();
    void setName(String name);
}
