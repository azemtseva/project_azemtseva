package entity;

public interface TransactionCategory {
    TransactionType getTransaction();
    void setTransaction(TransactionType TransactionI);

    CategoryModel getCategory();
    void setCategory(CategoryModel Category);
}
