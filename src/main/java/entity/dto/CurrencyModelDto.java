package entity.dto;

import entity.CurrencyModel;

import java.util.Objects;

public class CurrencyModelDto implements CurrencyModel {

    int id;
    String name;
    String code;
    String symbol;

    public CurrencyModelDto(int id, String name, String code, String symbol) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.symbol = symbol;
    }
    public CurrencyModelDto(String name, String code, String symbol) {

        this.name = name;
        this.code = code;
        this.symbol = symbol;
    }

    public CurrencyModelDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CurrencyModelDto that = (CurrencyModelDto) o;
        return id == that.id &&
                Objects.equals(name, that.name) &&
                Objects.equals(code, that.code) &&
                Objects.equals(symbol, that.symbol);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, code, symbol);
    }
}
