package entity.dto;

import entity.AccountModel;
import entity.CurrencyModel;

import java.util.Objects;

public class AccountModelDto implements AccountModel {
    int id;
    String name;
    Double amount;
    CurrencyModel currency;

    public AccountModelDto(int id, String name, Double amount, CurrencyModel currency) {
        this.id = id;
        this.name = name;
        this.amount = amount;
        this.currency = currency;
    }

    public AccountModelDto(String name, Double amount, CurrencyModel currency) {
        this.name = name;
        this.amount = amount;
        this.currency = currency;
    }

    public AccountModelDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public CurrencyModel getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyModel currency) {
        this.currency = currency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountModelDto that = (AccountModelDto) o;
        return id == that.id &&
                Objects.equals(name, that.name) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(currency, that.currency);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, amount, currency);
    }
}
