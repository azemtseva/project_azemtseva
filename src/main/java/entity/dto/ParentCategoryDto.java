package entity.dto;

import entity.ParentCategory;

public class ParentCategoryDto implements ParentCategory {
    int id;
    String name;

    public ParentCategoryDto(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public ParentCategoryDto(String name) {
        this.name = name;
    }

    public ParentCategoryDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
