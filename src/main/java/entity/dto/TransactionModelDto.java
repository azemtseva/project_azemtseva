package entity.dto;

import entity.AccountModel;
import entity.TransactionModel;
import entity.TransactionType;

public class TransactionModelDto implements TransactionModel {
    int id;
    String name;
    Double Amount;
    Boolean ProfitOrLoss;
    TransactionType Type;
    AccountModel Account;

    public TransactionModelDto(int id, String name, Double amount, Boolean profitOrLoss, TransactionType type, AccountModel account) {
        this.id = id;
        this.name = name;
        Amount = amount;
        ProfitOrLoss = profitOrLoss;
        Type = type;
        Account = account;
    }

    public TransactionModelDto(String name, Double amount, Boolean profitOrLoss, TransactionType type, AccountModel account) {
        this.name = name;
        Amount = amount;
        ProfitOrLoss = profitOrLoss;
        Type = type;
        Account = account;
    }

    public TransactionModelDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getAmount() {
        return Amount;
    }

    public void setAmount(Double amount) {
        Amount = amount;
    }

    public Boolean getProfitOrLoss() {
        return ProfitOrLoss;
    }

    public void setProfitOrLoss(Boolean profitOrLoss) {
        ProfitOrLoss = profitOrLoss;
    }

    public TransactionType getType() {
        return Type;
    }

    public void setType(TransactionType type) {
        Type = type;
    }

    public AccountModel getAccount() {
        return Account;
    }

    public void setAccount(AccountModel account) {
        Account = account;
    }
}
