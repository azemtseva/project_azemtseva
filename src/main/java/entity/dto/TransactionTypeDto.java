package entity.dto;

import entity.TransactionType;

public class TransactionTypeDto implements TransactionType {
    int id;
    String name;

    public TransactionTypeDto(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public TransactionTypeDto(String name) {
        this.name = name;
    }

    public TransactionTypeDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
