package entity.dto;

import entity.CategoryModel;
import entity.ParentCategory;

public class CategoryModelDto implements CategoryModel {
    int id;
    String name;
    ParentCategory parent;

    public CategoryModelDto(int id, String name, ParentCategory parent) {
        this.id = id;
        this.name = name;
        this.parent = parent;
    }

    public CategoryModelDto(String name, ParentCategory parent) {
        this.name = name;
        this.parent = parent;
    }

    public CategoryModelDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ParentCategory getParent() {
        return parent;
    }

    public void setParent(ParentCategory parent) {
        this.parent = parent;
    }
}
