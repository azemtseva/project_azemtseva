package entity.dto;

import entity.CategoryModel;
import entity.TransactionCategory;
import entity.TransactionType;

public class TransactionCategoryDto implements TransactionCategory {
    TransactionType Transaction;
    CategoryModel Category;

    public TransactionCategoryDto(TransactionType transaction, CategoryModel category) {
        Transaction = transaction;
        Category = category;
    }

    public TransactionCategoryDto() {
    }

    public TransactionType getTransaction() {
        return Transaction;
    }

    public void setTransaction(TransactionType transaction) {
        Transaction = transaction;
    }

    public CategoryModel getCategory() {
        return Category;
    }

    public void setCategory(CategoryModel category) {
        Category = category;
    }
}
