package entity;

public interface CategoryModel {
    int getId();
    void setId(int id);

    String getName();
    void setName(String name);

    ParentCategory getParent();
    void setParent(ParentCategory Parent);
}
