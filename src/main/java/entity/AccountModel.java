package entity;

public interface AccountModel {

    int getId();
    void setId(int id);

    String getName();
    void setName(String name);

    Double getAmount();
    void setAmount(Double amount);

    CurrencyModel getCurrency();
    void setCurrency(CurrencyModel currency);

}
