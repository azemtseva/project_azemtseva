package entity;

public interface CurrencyModel   {
    int getId();
    void setId(int id);

    String getName();
    void setName(String name);

    String getCode();
    void setCode(String code);

    String getSymbol();
    void setSymbol(String symbol);


}
