package dao;

import java.util.Collection;

public interface EntityDao<T> {
    T findByID(long id);

    Collection<T> findAll();

    void remove(long id);

    T save(T model);

    void update(T model);

}
