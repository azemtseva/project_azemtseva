package dao;

import JDBC.JDBConnect;
import entity.TransactionType;
import entity.dto.TransactionTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

@Component
public class TransactionTypeDao implements EntityDao<TransactionType> {

    JDBConnect jdbConnect;

    @Autowired
    public TransactionTypeDao(JDBConnect jdbConnect) {
        this.jdbConnect = jdbConnect;
    }

    public TransactionType findByID(long id) {
        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM currencymodel_tbl WHERE id = " + "'" + id + "'");
            TransactionType transactionType = new TransactionTypeDto();
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            transactionType.setId(resultSet.getInt(1));
            transactionType.setName(resultSet.getString(2));
            connection.close();
            return transactionType;
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
        return null;
    }

    public Collection<TransactionType> findAll() {
        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM currencymodel_tbl");
            ArrayList<TransactionType> transactionTypes = new ArrayList<>();
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                TransactionType transactionType = new TransactionTypeDto();
                transactionType.setId(resultSet.getInt(1));
                transactionType.setName(resultSet.getString(2));
                transactionTypes.add(transactionType);
            }
            connection.close();
            return transactionTypes;
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
        return null;
    }

    public void remove(long id) {
        try (Connection connection = jdbConnect.connectionDB()){
             PreparedStatement preparedStatementRemoveLinks = connection.prepareStatement
                    ("DELETE FROM transactionModel_tbl WHERE type_id = " + id);
            preparedStatementRemoveLinks.executeUpdate();
            /*PreparedStatement preparedStatementRemoveLinks = connection.prepareStatement
                    ("UPDATE transactionModel_tbl SET type_id=0 WHERE type_id = " + id);
            preparedStatementRemoveLinks.executeUpdate(); */
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM transaction_type_tbl WHERE id = " + id);
            preparedStatement.executeUpdate();
            connection.commit();
            connection.close();
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
    }

    public TransactionType save(TransactionType model) {
        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO transaction_type_tbl(name) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(2, model.getName());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                model.setId(resultSet.getInt(1));
            }
            connection.commit();
            connection.close();
            return model;
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
        return null;
    }

    public void update(TransactionType model) {
        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE currencymodel_tbl SET name = " + "'" + model.getName() + "'" +
                            "WHERE ID = " + model.getId());
            preparedStatement.executeUpdate();
            connection.commit();
            connection.close();
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
    }
}
