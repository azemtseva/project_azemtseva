package dao;

import JDBC.JDBConnect;
import entity.ParentCategory;
import entity.dto.ParentCategoryDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

@Component

public class ParentCategoryDao implements EntityDao<ParentCategory> {
    JDBConnect jdbConnect;

    @Autowired
    public ParentCategoryDao(JDBConnect jdbConnect) {
        this.jdbConnect = jdbConnect;
    }

    public ParentCategory findByID(long id) {
        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM parent_category_tbl WHERE id = " + "'" + id + "'");
            ParentCategory parentModel = new ParentCategoryDto();
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            parentModel.setId(resultSet.getInt(1));
            parentModel.setName(resultSet.getString(2));
            connection.close();
            return parentModel;
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
        return null;
    }

    public Collection<ParentCategory> findAll() {

        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM parent_category_tbl");
            ArrayList<ParentCategory> parentModels = new ArrayList<>();
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                ParentCategory parentModel = new ParentCategoryDto();
                parentModel.setId(resultSet.getInt(1));
                parentModel.setName(resultSet.getString(2));
                parentModels.add(parentModel);
            }
            connection.close();
            return parentModels;
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
        return null;
    }

    public void remove(long id) {
        try (Connection connection = jdbConnect.connectionDB()){

             PreparedStatement preparedStatementRemoveLinks = connection.prepareStatement
                    ("DELETE FROM categoryModel_tbl WHERE parent_id = " + id);
            preparedStatementRemoveLinks.executeUpdate();

            /*PreparedStatement preparedStatementRemoveLinks = connection.prepareStatement
                    ("UPDATE categoryModel_tbl SET parent_id=0 WHERE currency_id = " + id);
            preparedStatementRemoveLinks.executeUpdate(); */
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM parent_category_tbl WHERE id = " + id);
            preparedStatement.executeUpdate();
            connection.commit();
            connection.close();
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();
        }
    }

    public ParentCategory save(ParentCategory model) {
        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO parent_category_tbl (name) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, model.getName());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                model.setId(resultSet.getInt(1));
            }
            connection.commit();
            connection.close();
            return model;
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
        return null;
    }

    public void update(ParentCategory model) {
        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE parent_category_tbl SET name = " + "'" + model.getName() + "'" +
                            "WHERE ID = " + model.getId());
            preparedStatement.executeUpdate();
            connection.commit();
            connection.close();
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }

    }
}
