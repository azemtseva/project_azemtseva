package dao;

import JDBC.JDBConnect;
import entity.CategoryModel;
import entity.dto.CategoryModelDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
@Component
public class CategoryModelDao implements  EntityDao<CategoryModel> {
    JDBConnect jdbConnect;
    ParentCategoryDao parentCategoryDao;

    @Autowired
    public CategoryModelDao(JDBConnect jdbConnect, ParentCategoryDao parentCategoryDao) {
        this.jdbConnect = jdbConnect;
        this.parentCategoryDao = parentCategoryDao;
    }

    public CategoryModel findByID(long id) {

        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM categoryModel_tbl WHERE id = " + "'" + id + "'");
            CategoryModel categoryModel = new CategoryModelDto();
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            categoryModel.setId(resultSet.getInt(1));
            categoryModel.setName(resultSet.getString(2));
            categoryModel.setParent(parentCategoryDao.findByID(resultSet.getInt(3)));
            connection.close();
            return categoryModel;
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
        return null;
    }

    public Collection<CategoryModel> findAll() {
        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM categoryModel_tbl");
            ArrayList<CategoryModel> categoryModels = new ArrayList<>();
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                CategoryModel categoryModel = new CategoryModelDto();
                categoryModel.setId(resultSet.getInt(1));
                categoryModel.setName(resultSet.getString(2));
                categoryModel.setParent(parentCategoryDao.findByID(resultSet.getInt(3)));
                categoryModels.add(categoryModel);
            }
            connection.close();
            return categoryModels;
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
        return null;
    }

    public void remove(long id) {
        try (Connection connection = jdbConnect.connectionDB()){

             PreparedStatement preparedStatementRemoveLinks = connection.prepareStatement
                    ("DELETE FROM transaction_category_tbl WHERE category_id = " + id);
            preparedStatementRemoveLinks.executeUpdate();

            /*PreparedStatement preparedStatementRemoveLinks = connection.prepareStatement
                    ("UPDATE transaction_category_tbl SET currency_id=0 WHERE category_id = " + id);
            preparedStatementRemoveLinks.executeUpdate();*/
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM categoryModel_tbl WHERE id = " + id);
            preparedStatement.executeUpdate();
            connection.commit();
            connection.close();
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
    }

    public CategoryModel save(CategoryModel model) {

        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO categoryModel_tbl (name, parent_id) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, model.getName());
            preparedStatement.setInt(2, model.getParent().getId());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                model.setId(resultSet.getInt(1));
            }
            connection.commit();
            connection.close();
            return model;
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
        return null;
    }

    public void update(CategoryModel model) {
        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE categoryModel_tbl SET name = " + "'" + model.getName() + "'" +
                            ", parent_id = " + "'" + model.getParent().getId() + "'"  +
                            "WHERE ID = " + model.getId());
            preparedStatement.executeUpdate();
            connection.commit();
            connection.close();
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
    }
}
