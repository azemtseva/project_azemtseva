package dao;

import JDBC.JDBConnect;
import entity.TransactionModel;
import entity.dto.TransactionModelDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
@Component
public class TransactionModelDao implements EntityDao<TransactionModel> {
    JDBConnect jdbConnect;
    TransactionTypeDao transactionTypeDao;
    AccountModelDao accountModelDao;

    @Autowired
    public TransactionModelDao(JDBConnect jdbConnect, TransactionTypeDao transactionTypeDao, AccountModelDao accountModelDao ) {
        this.jdbConnect = jdbConnect;
        this.transactionTypeDao = transactionTypeDao;
        this.accountModelDao = accountModelDao;
    }


    public TransactionModel findByID(long id) {

        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM transactionModel_tbl WHERE id = " + "'" + id + "'");
            TransactionModel transactionModel = new TransactionModelDto();
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            transactionModel.setId(resultSet.getInt(1));
            transactionModel.setName(resultSet.getString(2));
            transactionModel.setAmount(resultSet.getDouble(3));
            transactionModel.setProfitOrLoss(resultSet.getBoolean(4));
            transactionModel.setType(transactionTypeDao.findByID(resultSet.getInt(5)));
            transactionModel.setAccount(accountModelDao.findByID(resultSet.getInt(6)));
            connection.close();
            return transactionModel;
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
        return null;
    }

    public Collection<TransactionModel> findAll() {
        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM transactionModel_tbl");
            ArrayList<TransactionModel> transactionModels = new ArrayList<>();
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                TransactionModel transactionModel = new TransactionModelDto();
                transactionModel.setId(resultSet.getInt(1));
                transactionModel.setName(resultSet.getString(2));
                transactionModel.setAmount(resultSet.getDouble(3));
                transactionModel.setProfitOrLoss(resultSet.getBoolean(4));
                transactionModel.setType(transactionTypeDao.findByID(resultSet.getInt(5)));
                transactionModel.setAccount(accountModelDao.findByID(resultSet.getInt(6)));
                transactionModels.add(transactionModel);
            }
            connection.close();
            return transactionModels;
        } catch (SQLException | ClassNotFoundException e) {


            e.printStackTrace();

        }
        return null;
    }

    public void remove(long id) {
        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM transactionModel_tbl WHERE id = " + id);
            preparedStatement.executeUpdate();
            connection.commit();
            connection.close();
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();
        }
    }

    public TransactionModel save(TransactionModel model) {
        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO transactionModel_tbl (name, amount, profitOrLoss, type_id, account_id) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(2, model.getName());
            preparedStatement.setDouble(3, model.getAmount());
            preparedStatement.setBoolean(4, model.getProfitOrLoss());
            preparedStatement.setInt(5,model.getType().getId());
            preparedStatement.setInt(6,model.getAccount().getId());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                model.setId(resultSet.getInt(1));
            }
            connection.commit();
            connection.close();
            return model;
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
        return null;
    }

    public void update(TransactionModel model) {
        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE transactionModel_tbl SET name = " + "'" + model.getName() + "'" +
                            ", amount = " + "'" + model.getAmount() + "'" +
                            ", profitOrLoss = " + "'" + model.getProfitOrLoss() + "'" +
                            ", type_id = " + "'" + model.getType().getId() + "'" +
                            ", account_id = " + "'" + model.getAccount().getId() + "'" +
                            "WHERE ID = " + model.getId());
            preparedStatement.executeUpdate();
            connection.commit();
            connection.close();
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }


    }
}
