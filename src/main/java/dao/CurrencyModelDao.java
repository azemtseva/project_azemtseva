package dao;

import JDBC.JDBConnect;
import entity.CurrencyModel;
import entity.dto.CurrencyModelDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

@Component
public class CurrencyModelDao implements EntityDao<CurrencyModel> {


    JDBConnect jdbConnect;
    @Autowired
    public CurrencyModelDao(JDBConnect jdbConnect) {
        this.jdbConnect = jdbConnect;
    }

    public CurrencyModel findByID(long id) {
        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM currencymodel_tbl WHERE id = " + "'" + id + "'");
            CurrencyModel currencyModel = new CurrencyModelDto();
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            currencyModel.setId(resultSet.getInt(1));
            currencyModel.setName(resultSet.getString(2));
            currencyModel.setCode(resultSet.getString(3));
            currencyModel.setSymbol(resultSet.getString(4));
            connection.close();
            return currencyModel;
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
        return null;
    }

    public Collection<CurrencyModel> findAll() {
        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM currencymodel_tbl");
            ArrayList<CurrencyModel> currencyModels = new ArrayList<>();
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                CurrencyModel currencyModel = new CurrencyModelDto();
                currencyModel.setId(resultSet.getInt(1));
                currencyModel.setName(resultSet.getString(2));
                currencyModel.setCode(resultSet.getString(3));
                currencyModel.setSymbol(resultSet.getString(4));
                currencyModels.add(currencyModel);
            }
            connection.close();
            return currencyModels;
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
        return null;

    }

    public void remove(long id) {
        try (Connection connection = jdbConnect.connectionDB()){

             PreparedStatement preparedStatementRemoveLinks = connection.prepareStatement
                    ("DELETE FROM accountModel_tbl WHERE currency_id = " + id);
            preparedStatementRemoveLinks.executeUpdate();
             /*
            PreparedStatement preparedStatementRemoveLinks = connection.prepareStatement
                    ("UPDATE accountModel_tbl SET currency_id=0 WHERE currency_id = " + id);
            preparedStatementRemoveLinks.executeUpdate();*/
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM currencymodel_tbl WHERE id = " + id);
            preparedStatement.executeUpdate();
            connection.commit();
            connection.close();
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }

    }

    public CurrencyModel save(CurrencyModel model) {
        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO currencyModel_tbl (name, code, symbol) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, model.getName());
            preparedStatement.setString(2, model.getCode());
            preparedStatement.setString(3, model.getSymbol());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                model.setId(resultSet.getInt(1));
            }
            connection.commit();
            connection.close();
            return model;
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
        return null;
    }

    public void update(CurrencyModel model) {
        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE currencymodel_tbl SET name = " + "'" + model.getName() + "'" +
                            ", code = " + "'" + model.getCode() + "'" +
                            ", symbol = " + "'" + model.getSymbol() + "'" +
                            "WHERE ID = " + model.getId());
            preparedStatement.executeUpdate();
            connection.commit();
            connection.close();
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }

    }
}
