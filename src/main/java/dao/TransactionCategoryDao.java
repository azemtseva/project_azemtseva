package dao;

import JDBC.JDBConnect;
import entity.TransactionCategory;
import entity.dto.TransactionCategoryDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

@Component
public class TransactionCategoryDao {
    JDBConnect jdbConnect;
    CategoryModelDao categoryModelDao;
    TransactionTypeDao transactionTypeDao;

    @Autowired
    public TransactionCategoryDao(JDBConnect jdbConnect, CategoryModelDao categoryModelDao, TransactionTypeDao transactionTypeDao) {
        this.jdbConnect = jdbConnect;
        this.categoryModelDao = categoryModelDao;
        this.transactionTypeDao = transactionTypeDao;

    }

    public TransactionCategory findByIDs(long transactionId, long categoryId) {
        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM transaction_category_tbl WHERE transaction_id = " + "'" + transactionId + "'"
                            + "and category_id = " + "'" + categoryId + "'");
            TransactionCategory transactionCategory = new TransactionCategoryDto();
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            transactionCategory.setTransaction(transactionTypeDao.findByID(resultSet.getInt(1)));
            transactionCategory.setCategory(categoryModelDao.findByID(resultSet.getInt(2)));
            connection.close();
            return transactionCategory;
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
        return null;
    }

    public Collection<TransactionCategory> findAll() {
        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM transaction_category_tbl ");
            ArrayList<TransactionCategory> transactionCategories = new ArrayList<>();
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                TransactionCategory transactionCategory = new TransactionCategoryDto();
                transactionCategory.setTransaction(transactionTypeDao.findByID(resultSet.getInt(1)));
                transactionCategory.setCategory(categoryModelDao.findByID(resultSet.getInt(2)));
                transactionCategories.add(transactionCategory);
            }
            connection.close();
            return transactionCategories;
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
        return null;
    }

    public void removeIds(long transactionId, long categoryId) {
        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM transaction_category_tbl WHERE transaction_id = " + "'" + transactionId + "'"
                            + "and category_id = " + "'" + categoryId + "'");
            preparedStatement.executeUpdate();
            connection.commit();
            connection.close();
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();
        }
    }

    public TransactionCategory save(TransactionCategory model) {
        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO transaction_category_tbl (transaction_id, category_id) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, model.getTransaction().getId());
            preparedStatement.setInt(2, model.getCategory().getId());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            connection.commit();
            connection.close();
            return model;
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
        return null;
    }

    public void update(TransactionCategory model) {
        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE transaction_category_tbl SET transaction_id = " + "'" + model.getTransaction().getId() + "'" +
                            ", category_id = " + "'" +model.getCategory().getId() + "'" +
                            "WHERE category_id = " + model.getCategory().getId() + "'" +
                            " and transaction_id = " + model.getTransaction().getId());
            preparedStatement.executeUpdate();
            connection.commit();
            connection.close();
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
    }
}
