package dao;

import JDBC.JDBConnect;
import entity.AccountModel;
import entity.dto.AccountModelDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

@Component
public class AccountModelDao implements EntityDao<AccountModel> {
    JDBConnect jdbConnect;
    CurrencyModelDao currencyModelDao;

    @Autowired
    public AccountModelDao(JDBConnect jdbConnect, CurrencyModelDao currencyModelDao) {
        this.jdbConnect = jdbConnect;
        this.currencyModelDao = currencyModelDao;
    }

    public AccountModel findByID(long id) {
        try (Connection connection = jdbConnect.connectionDB()){
            
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM accountModel_tbl WHERE id = " + "'" + id + "'");
            AccountModel accountModel = new AccountModelDto();
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            accountModel.setId(resultSet.getInt(1));
            accountModel.setName(resultSet.getString(2));
            accountModel.setAmount(resultSet.getDouble(3));
            accountModel.setCurrency(currencyModelDao.findByID(resultSet.getInt(4)));
            
            return accountModel;
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
        return null;
    }

    public Collection<AccountModel> findAll() {
        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM accountModel_tbl");
            ArrayList<AccountModel> accountModels = new ArrayList<>();
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                AccountModel accountModel = new AccountModelDto();
                accountModel.setId(resultSet.getInt(1));
                accountModel.setName(resultSet.getString(2));
                accountModel.setAmount(resultSet.getDouble(3));
                accountModel.setCurrency(currencyModelDao.findByID(resultSet.getInt(4)));
                accountModels.add(accountModel);
            }
            
            return accountModels;
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
        return null;
    }

    public void remove(long id) {
        try (Connection connection = jdbConnect.connectionDB()){

             PreparedStatement preparedStatementRemoveLinks = connection.prepareStatement
                    ("DELETE FROM transactionModel_tbl WHERE account_id = " + id);
            preparedStatementRemoveLinks.executeUpdate();

           /* PreparedStatement preparedStatementRemoveLinks = connection.prepareStatement
                    ("UPDATE transactionModel_tbl SET account_id=0 WHERE account_id = " + id);
            preparedStatementRemoveLinks.executeUpdate();*/
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM accountModel_tbl WHERE id = " + id);
            preparedStatement.executeUpdate();
            connection.commit();
            
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();
        }
    }

    public AccountModel save(AccountModel model) {
        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO accountmodel_tbl (NAME, AMOUNT, CURRENCY_ID) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, model.getName());
            preparedStatement.setDouble(2, model.getAmount());
            preparedStatement.setInt(3, model.getCurrency().getId());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                model.setId(resultSet.getInt(1));
            }
            connection.commit();
            
            return model;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();

        }
        return null;

    }

    public void update(AccountModel model) {
        try (Connection connection = jdbConnect.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE accountmodel_tbl SET name = " + "'" + model.getName() + "'" +
                            ", AMOUNT = " + "'" + model.getAmount() + "'" +
                            ", CURRENCY_ID = " + "'" + model.getCurrency().getId() + "'" +
                            "WHERE ID = " + model.getId());
            preparedStatement.executeUpdate();
            connection.commit();
            
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
    }
}
