package moks;

import dao.EntityDao;
import entity.ParentCategory;
import entity.dto.ParentCategoryDto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ParentCategoryDaoMok implements EntityDao<ParentCategory> {
    @Override
    public ParentCategory findByID(long id) {
        ParentCategory parentCategory = new ParentCategoryDto((int)id,"test");
        return parentCategory;
    }

    @Override
    public Collection<ParentCategory> findAll() {
        List<ParentCategory> parentCategories = new ArrayList<>();
        parentCategories.add( new ParentCategoryDto(5,"name"));
        parentCategories.add( new ParentCategoryDto(6,"name"));
        parentCategories.add( new ParentCategoryDto(7,"name"));
        return parentCategories;
    }

    @Override
    public void remove(long id) {

    }

    @Override
    public ParentCategory save(ParentCategory model) {
        return model;
    }

    @Override
    public void update(ParentCategory model) {

    }
}
