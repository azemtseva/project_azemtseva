package moks;

import dao.EntityDao;
import entity.TransactionModel;
import entity.dto.AccountModelDto;
import entity.dto.CurrencyModelDto;
import entity.dto.TransactionModelDto;
import entity.dto.TransactionTypeDto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TransactionModelDaoMok implements EntityDao<TransactionModel> {
    @Override
    public TransactionModel findByID(long id) {
        TransactionModel transactionModel = new TransactionModelDto((int) id, "test",10000.0,false,
                new TransactionTypeDto(0,"test"),new AccountModelDto(0,"name",1000.0,
                new CurrencyModelDto(0,"name","test","t")));
        return transactionModel;
    }

    @Override
    public Collection<TransactionModel> findAll() {
        List<TransactionModel> transactionModelList = new ArrayList<>();
        transactionModelList.add(new TransactionModelDto(5, "test",10000.0,false,
                new TransactionTypeDto(0,"test"),new AccountModelDto(0,"name",1000.0,
                new CurrencyModelDto(0,"name","test","t"))));
        transactionModelList.add(new TransactionModelDto(6, "test",10000.0,false,
                new TransactionTypeDto(0,"test"),new AccountModelDto(0,"name",1000.0,
                new CurrencyModelDto(0,"name","test","t"))));
        transactionModelList.add(new TransactionModelDto(7, "test",10000.0,false,
                new TransactionTypeDto(0,"test"),new AccountModelDto(0,"name",1000.0,
                new CurrencyModelDto(0,"name","test","t"))));
        return transactionModelList;
    }

    @Override
    public void remove(long id) {

    }

    @Override
    public TransactionModel save(TransactionModel model) {
        return model;
    }

    @Override
    public void update(TransactionModel model) {

    }
}
