package moks;

import dao.EntityDao;
import entity.CategoryModel;
import entity.dto.CategoryModelDto;
import entity.dto.ParentCategoryDto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CategoryModelDaoMok implements EntityDao<CategoryModel> {
    @Override
    public CategoryModel findByID(long id) {
        CategoryModel categoryModel = new CategoryModelDto((int)id,"test", new ParentCategoryDto(0,"test"));
        return categoryModel;
    }

    @Override
    public Collection<CategoryModel> findAll() {
        List<CategoryModel> categoryModels = new ArrayList<>();
        categoryModels.add(new CategoryModelDto(6,"test", new ParentCategoryDto(0,"test")));
        categoryModels.add(new CategoryModelDto(7,"test", new ParentCategoryDto(0,"test")));
        categoryModels.add(new CategoryModelDto(8,"test", new ParentCategoryDto(0,"test")));
        return categoryModels;
    }

    @Override
    public void remove(long id) {

    }

    @Override
    public CategoryModel save(CategoryModel model) {
        return model;
    }

    @Override
    public void update(CategoryModel model) {

    }
}
