package moks;

import dao.EntityDao;
import entity.*;
import entity.dto.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TransactionCategoryDaoMok implements EntityDao<TransactionCategory> {
    @Override
    public TransactionCategory findByID(long id) {
        CurrencyModel currencyModel = new  CurrencyModelDto(0, "test", "test", "test");
        AccountModel accountModel = new AccountModelDto(0, "name", 100000.0,currencyModel);
        TransactionType transactionType = new TransactionTypeDto(0, "test");
        TransactionModel transactionModel = new TransactionModelDto(0, "test", 1000000.0, false, transactionType,accountModel);
        CategoryModel categoryModel = new CategoryModelDto(0,"test", new ParentCategoryDto(0,"test"));
        TransactionCategory transactionCategory = new TransactionCategoryDto(transactionType,categoryModel);
        return transactionCategory;
    }

    @Override
    public Collection<TransactionCategory> findAll() {
        CurrencyModel currencyModel = new  CurrencyModelDto(0, "test", "test", "test");
        AccountModel accountModel = new AccountModelDto(0, "name", 100000.0,currencyModel);
        TransactionType transactionType = new TransactionTypeDto(0, "test");
        TransactionModel transactionModel = new TransactionModelDto(0, "test", 1000000.0, false, transactionType,accountModel);
        CategoryModel categoryModel = new CategoryModelDto(0,"test", new ParentCategoryDto(0,"test"));
        TransactionCategory transactionCategory = new TransactionCategoryDto(transactionType,categoryModel);
        List<TransactionCategory> transactionCategorys = new ArrayList<>();
        transactionCategorys.add( new TransactionCategoryDto(transactionType,categoryModel));
        return transactionCategorys;
    }

    @Override
    public void remove(long id) {

    }

    @Override
    public TransactionCategory save(TransactionCategory model) {
        return model;
    }

    @Override
    public void update(TransactionCategory model) {

    }
}
