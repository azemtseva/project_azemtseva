package moks;

import dao.EntityDao;
import entity.AccountModel;
import entity.dto.AccountModelDto;
import entity.dto.CurrencyModelDto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class AccountModelDaoMok implements EntityDao<AccountModel> {
    @Override
    public AccountModel findByID(long id) {
        AccountModel accountModel = new AccountModelDto((int) id, "name", 100000.0, new CurrencyModelDto(0, "test", "test", "test"));
        return accountModel;
    }

    @Override
    public Collection<AccountModel> findAll() {
        List<AccountModel> accountModelList = new ArrayList<>();
        accountModelList.add(new AccountModelDto(5, "name", 100000.0, new CurrencyModelDto(0, "test", "test", "test")));
        accountModelList.add(new AccountModelDto(6, "name", 100000.0, new CurrencyModelDto(0, "test", "test", "test")));
        accountModelList.add(new AccountModelDto(7, "name", 100000.0, new CurrencyModelDto(0, "test", "test", "test")));
        return accountModelList;
    }

    @Override
    public void remove(long id) {

    }

    @Override
    public AccountModel save(AccountModel model) {
        return model;
    }

    @Override
    public void update(AccountModel model) {
    }
}
