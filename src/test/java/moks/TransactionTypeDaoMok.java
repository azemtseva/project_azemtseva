package moks;

import dao.EntityDao;
import entity.TransactionType;
import entity.dto.TransactionTypeDto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TransactionTypeDaoMok implements EntityDao<TransactionType> {
    @Override
    public TransactionType findByID(long id) {
        TransactionType transactionType = new TransactionTypeDto((int)id,"test");
        return transactionType;
    }

    @Override
    public Collection<TransactionType> findAll() {
        List<TransactionType> transactionTypeList = new ArrayList<>();
        transactionTypeList.add( new TransactionTypeDto(5,"name"));
        transactionTypeList.add( new TransactionTypeDto(6,"name"));
        transactionTypeList.add( new TransactionTypeDto(7,"name"));
        return transactionTypeList;
    }

    @Override
    public void remove(long id) {

    }

    @Override
    public TransactionType save(TransactionType model) {
        return model;
    }

    @Override
    public void update(TransactionType model) {

    }
}
