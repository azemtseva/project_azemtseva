package moks;

import Exceptions.CustomExceptions;
import org.springframework.stereotype.Component;

import java.sql.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Component
public class JDBCMok {

        public Connection conn;
        public Statement statmt;
        public ResultSet resSet;

        // --------ПОДКЛЮЧЕНИЕ К БАЗЕ ДАННЫХ--------
        public Connection connectionDB() throws ClassNotFoundException, SQLException {
            try {
                conn = null;
                conn = DriverManager.getConnection("jdbc:h2:tcp://localhost/~/test");
                conn.setAutoCommit(false);
                return conn;
            }   catch (SQLException e) {
                throw  new CustomExceptions("Error in ConnectionSupplier", e);
            }


        }


        /*
            // -------- Вывод таблицы--------
            public void ReadDB() throws ClassNotFoundException, SQLException {
                resSet = statmt.executeQuery("SELECT * FROM users");

                while (resSet.next()) {
                    int id = resSet.getInt("id");
                    String name = resSet.getString("name");
                    String phone = resSet.getString("phone");
                    System.out.println("ID = " + id);
                    System.out.println("name = " + name);
                    System.out.println("phone = " + phone);
                    System.out.println();
                }

                System.out.println("Таблица выведена");
            }
        */
        // --------Закрытие--------
        public void CloseDB() throws ClassNotFoundException, SQLException {
            conn.close();
            statmt.close();
            resSet.close();

            System.out.println("Соединения закрыты");
        }
    }


