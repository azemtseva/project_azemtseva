package moks;

import dao.EntityDao;
import entity.CurrencyModel;
import entity.dto.CurrencyModelDto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CurrencyModelDaoMok implements EntityDao<CurrencyModel> {
    @Override
    public CurrencyModel findByID(long id) {
        CurrencyModel currencyModel = new CurrencyModelDto((int)id, "test","test","t");
        return currencyModel;
    }

    @Override
    public Collection<CurrencyModel> findAll() {
        List<CurrencyModel> currencyModels = new ArrayList<>();
        currencyModels.add( new CurrencyModelDto(5, "test","test","t"));
        currencyModels.add( new CurrencyModelDto(6, "test","test","t"));
        currencyModels.add( new CurrencyModelDto(7, "test","test","t"));
        return currencyModels;
    }

    @Override
    public void remove(long id) {

    }

    @Override
    public CurrencyModel save(CurrencyModel model) {
        return null;
    }

    @Override
    public void update(CurrencyModel model) {

    }
}
