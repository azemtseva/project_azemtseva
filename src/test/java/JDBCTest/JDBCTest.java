package JDBCTest;

import JDBC.JDBConnect;
import moks.JDBCMok;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

public class JDBCTest {
    JDBConnect jdbConnect;

    @Before
    public void setUp() {
        jdbConnect = new JDBConnect();

    }

    @Test
    public void testConnection() {
        try {
            Connection connection = jdbConnect.connectionDB();
            Assert.assertTrue(connection.isValid(6*1000));
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

}
