package dao;

import JDBCTest.JDBCTest;
import entity.CurrencyModel;
import entity.dto.CurrencyModelDto;
import moks.JDBCMok;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.*;

public class CurrencyModelDaoTest {
    JDBCMok jdbcTest;

    public CurrencyModelDaoTest() {
    }

    @Before
    public void setUp() {
        jdbcTest = new JDBCMok();
    }
    @Test
    public void findByID() {
        
        int id = 2;
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM currencyModel_tbl WHERE id = " + "'" + id + "'");
            ResultSet resultSet = preparedStatement.executeQuery();
            Assert.assertTrue(resultSet.next());
            
        } catch (SQLException | ClassNotFoundException e) {
            
                e.printStackTrace();

            }     
    }

    @Test
    public void findAll() {
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM currencyModel_tbl");
            ResultSet resultSet = preparedStatement.executeQuery();
            Assert.assertTrue(resultSet.next());
            
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
    }

    @Test
    public void remove() {

        int id = 5;
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement2 = connection.prepareStatement
                    ("INSERT INTO currencyModel_tbl (id,name, code, symbol) VALUES (5,'ДолларТЕСТ', '5', '$')", Statement.RETURN_GENERATED_KEYS);
            preparedStatement2.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM currencyModel_tbl WHERE id = " + id);
            preparedStatement.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement1 = connection.prepareStatement
                    ("SELECT * FROM currencyModel_tbl WHERE id = " + "'" + id + "'");
            ResultSet resultSet = preparedStatement1.executeQuery();
            Assert.assertFalse(resultSet.next());
            
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();
        }
    }

    @Test
    public void save() {
        try (Connection connection = jdbcTest.connectionDB()){

            PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO currencyModel_tbl (id,name, code, symbol) VALUES (5,'ДолларТЕСТ', '5', '$')", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement1 = connection.prepareStatement
                    ("SELECT * FROM currencyModel_tbl WHERE id = " + "'" + 5 + "'");
            ResultSet resultSet = preparedStatement1.executeQuery();
            Assert.assertTrue(resultSet.next());
            
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
    }

    @Test
    public void update() {

        int id = 5;
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatementI = connection.prepareStatement
                    ("INSERT INTO currencyModel_tbl (id,name, code, symbol) VALUES (5,'ДолларТЕСТ', '5', '$')", Statement.RETURN_GENERATED_KEYS);
            preparedStatementI.executeUpdate();
            connection.commit();
            CurrencyModel currencyModel = new CurrencyModelDto(5, "Тест1", "6","Р");
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE currencymodel_tbl SET name = " + "'" + currencyModel.getName() + "'" +
                            ", code = " + "'" + currencyModel.getCode() + "'" +
                            ", symbol = " + "'" + currencyModel.getSymbol() + "'" +
                            "WHERE ID = " + currencyModel.getId());
            preparedStatement.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement1 = connection.prepareStatement
                    ("SELECT * FROM currencymodel_tbl WHERE id = " + "'" + currencyModel.getId() + "'");
            CurrencyModel currencyModelUpdated = new CurrencyModelDto();
            ResultSet resultSet = preparedStatement1.executeQuery();
            resultSet.next();
            currencyModelUpdated.setId(resultSet.getInt(1));
            currencyModelUpdated.setName(resultSet.getString(2));
            currencyModelUpdated.setCode(resultSet.getString(3));
            currencyModelUpdated.setSymbol(resultSet.getString(4));
            
            Assert.assertEquals(currencyModel, currencyModelUpdated);
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
    }
}