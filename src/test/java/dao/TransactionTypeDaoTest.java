package dao;

import entity.TransactionType;
import entity.dto.TransactionTypeDto;
import moks.JDBCMok;
import org.junit.Test;


import JDBCTest.JDBCTest;
import org.junit.Assert;
import org.junit.Before;

import java.sql.*;

public class TransactionTypeDaoTest {
    JDBCMok jdbcTest;

    public TransactionTypeDaoTest() {
    }

    @Before
    public void setUp() {
        jdbcTest = new JDBCMok();
    }

    @Test
    public void findByID() {
        
        int id = 2;
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM transaction_type_tbl WHERE id = " + "'" + id + "'");
            ResultSet resultSet = preparedStatement.executeQuery();
            Assert.assertTrue(resultSet.next());
            
        } catch (SQLException | ClassNotFoundException e) {

                e.printStackTrace();


        }
    }

    @Test
    public void findAll() {
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM transaction_type_tbl");
            ResultSet resultSet = preparedStatement.executeQuery();
            Assert.assertTrue(resultSet.next());
            
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
    }

    @Test
    public void remove() {

        int id = 5;
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement2 = connection.prepareStatement
                    ("INSERT INTO transaction_type_tbl(id,name)\n" +
                            "VALUES (5, 'Test');", Statement.RETURN_GENERATED_KEYS);
            preparedStatement2.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM transaction_type_tbl WHERE id = " + id);
            preparedStatement.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement1 = connection.prepareStatement
                    ("SELECT * FROM transaction_type_tbl WHERE id = " + "'" + id + "'");
            ResultSet resultSet = preparedStatement1.executeQuery();
            Assert.assertFalse(resultSet.next());
            
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();
        }
    }

    @Test
    public void save() {
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO transaction_type_tbl(id,name)\n" +
                            "VALUES (5, 'Test');", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement1 = connection.prepareStatement
                    ("SELECT * FROM transaction_type_tbl WHERE id = " + "'" + 5 + "'");
            ResultSet resultSet = preparedStatement1.executeQuery();
            Assert.assertTrue(resultSet.next());
            
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
    }

    @Test
    public void update() {

        int id = 5;
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatementI = connection.prepareStatement
                    ("INSERT INTO transaction_type_tbl(id,name)\n" +
                            "VALUES (5, 'Test');", Statement.RETURN_GENERATED_KEYS);
            preparedStatementI.executeUpdate();
            connection.commit();
            TransactionType transactionType = new TransactionTypeDto(5, "Тест1");
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE transaction_type_tbl SET name = " + "'" + transactionType.getName() + "'" +
                            "WHERE ID = " + transactionType.getId());
            preparedStatement.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement1 = connection.prepareStatement
                    ("SELECT * FROM transaction_type_tbl WHERE id = " + "'" + transactionType.getId() + "'");
            TransactionType transactionTypeUpdate = new TransactionTypeDto();
            ResultSet resultSet = preparedStatement1.executeQuery();
            resultSet.next();
            transactionTypeUpdate.setId(resultSet.getInt(1));
            transactionTypeUpdate.setName(resultSet.getString(2));
            
            Assert.assertEquals(transactionType, transactionTypeUpdate);
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
    }
}