package dao;

import JDBCTest.JDBCTest;
import entity.AccountModel;
import entity.dto.AccountModelDto;
import entity.dto.CurrencyModelDto;
import moks.JDBCMok;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.*;

public class AccountModelDaoTest {
    public JDBCMok jdbcTest;

    public AccountModelDaoTest() {
    }

    @Before
    public void setUp() {
        jdbcTest = new JDBCMok();
    }

    @Test
    public void findByID() {
        
        int id = 2;
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM accountModel_tbl WHERE id = " + "'" + id + "'");
            ResultSet resultSet = preparedStatement.executeQuery();
            Assert.assertTrue(resultSet.next());
         
        } catch (SQLException | ClassNotFoundException e) {
           
                e.printStackTrace();

            }
        
    }

    @Test
    public void findAll() {

        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM accountModel_tbl");
            ResultSet resultSet = preparedStatement.executeQuery();
            Assert.assertTrue(resultSet.next());
         
        } catch (SQLException | ClassNotFoundException e) {


            e.printStackTrace();

        }
    }

    @Test
    public void remove() {

        int id = 5;
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement2 = connection.prepareStatement
                    ("INSERT INTO accountmodel_tbl (id,NAME, AMOUNT, CURRENCY_ID) VALUES (5,'TEst',5000000,1)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement2.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM accountModel_tbl WHERE id = " + id);
            preparedStatement.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement1 = connection.prepareStatement
                    ("SELECT * FROM accountModel_tbl WHERE id = " + "'" + id + "'");
            ResultSet resultSet = preparedStatement1.executeQuery();
            Assert.assertFalse(resultSet.next());
         
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();
        }
    }

    @Test
    public void save() {
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO accountmodel_tbl (id,NAME, AMOUNT, CURRENCY_ID) VALUES (5,'TEst',5000000,1)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement1 = connection.prepareStatement
                    ("SELECT * FROM accountModel_tbl WHERE id = " + "'" + 5 + "'");
            ResultSet resultSet = preparedStatement1.executeQuery();
            Assert.assertTrue(resultSet.next());
         
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }

    }

    @Test
    public void update() {

        int id = 5;
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatementI = connection.prepareStatement
                    ("INSERT INTO accountmodel_tbl (id,NAME, AMOUNT, CURRENCY_ID) VALUES (5,'TEst',5000000,1)", Statement.RETURN_GENERATED_KEYS);
            preparedStatementI.executeUpdate();
            connection.commit();
            AccountModel accountModel = new AccountModelDto(5, "Тест1", 0D, new CurrencyModelDto(3, "", "", ""));
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE accountmodel_tbl SET name = " + "'" + accountModel.getName() + "'" +
                            ", AMOUNT = " + "'" + accountModel.getAmount() + "'" +
                            ", CURRENCY_ID = " + "'" + accountModel.getCurrency().getId() + "'" +
                            "WHERE ID = " + accountModel.getId());
            preparedStatement.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement1 = connection.prepareStatement
                    ("SELECT * FROM accountModel_tbl WHERE id = " + "'" + accountModel.getId() + "'");
            AccountModel accountModelUpdated = new AccountModelDto();
            ResultSet resultSet = preparedStatement1.executeQuery();
            resultSet.next();
            accountModelUpdated.setId(resultSet.getInt(1));
            accountModelUpdated.setName(resultSet.getString(2));
            accountModelUpdated.setAmount(resultSet.getDouble(3));
            accountModelUpdated.setCurrency(new CurrencyModelDto(resultSet.getInt(4), "", "", ""));
         
            Assert.assertEquals(accountModel, accountModelUpdated);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();

        }
    }
}