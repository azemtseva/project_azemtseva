package dao;

import JDBCTest.JDBCTest;
import entity.ParentCategory;
import entity.dto.ParentCategoryDto;
import moks.JDBCMok;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.*;

public class ParentCategoryDaoTest {
    JDBCMok jdbcTest;
    public ParentCategoryDaoTest() {
    }

    @Before
    public void setUp() {
        jdbcTest = new JDBCMok();
    }
    @Test
    public void findByID() {
        
        int id = 2;
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM parent_category_tbl WHERE id = " + "'" + id + "'");
            ResultSet resultSet = preparedStatement.executeQuery();
            Assert.assertTrue(resultSet.next());
            
        } catch (SQLException | ClassNotFoundException e) {

                e.printStackTrace();

            }

    }

    @Test
    public void findAll() {
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM parent_category_tbl");
            ResultSet resultSet = preparedStatement.executeQuery();
            Assert.assertTrue(resultSet.next());
            
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
    }

    @Test
    public void remove() {

        int id = 4;
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement2 = connection.prepareStatement
                    ("INSERT INTO parent_category_tbl (id,name)\n" +
                            "VALUES (4,'Test');", Statement.RETURN_GENERATED_KEYS);
            preparedStatement2.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM parent_category_tbl WHERE id = " + id);
            preparedStatement.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement1 = connection.prepareStatement
                    ("SELECT * FROM parent_category_tbl WHERE id = " + "'" + id + "'");
            ResultSet resultSet = preparedStatement1.executeQuery();
            Assert.assertFalse(resultSet.next());
            
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();
        }
    }

    @Test
    public void save() {
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO parent_category_tbl (id,name)\n" +
                            "VALUES (4,'Test');", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement1 = connection.prepareStatement
                    ("SELECT * FROM parent_category_tbl WHERE id = " + "'" + 5 + "'");
            ResultSet resultSet = preparedStatement1.executeQuery();
            Assert.assertTrue(resultSet.next());
            
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
    }

    @Test
    public void update() {

        int id = 4;
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatementI = connection.prepareStatement
                    ("INSERT INTO parent_category_tbl (id,name)\n" +
                            "VALUES (4,'Test');", Statement.RETURN_GENERATED_KEYS);
            preparedStatementI.executeUpdate();
            connection.commit();
            ParentCategory parentCategory = new ParentCategoryDto(5, "Тест1");
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE parent_category_tbl SET name = " + "'" + parentCategory.getName() + "'" +
                            "WHERE ID = " + parentCategory.getId());
            preparedStatement.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement1 = connection.prepareStatement
                    ("SELECT * FROM currencymodel_tbl WHERE id = " + "'" + parentCategory.getId() + "'");
            ParentCategory parentCategoryUpdated = new ParentCategoryDto();
            ResultSet resultSet = preparedStatement1.executeQuery();
            resultSet.next();
            parentCategoryUpdated.setId(resultSet.getInt(1));
            parentCategoryUpdated.setName(resultSet.getString(2));
            
            Assert.assertEquals(parentCategory, parentCategoryUpdated);
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
    }
}