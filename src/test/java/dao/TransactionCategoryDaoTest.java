package dao;

import JDBCTest.JDBCTest;
import entity.*;
import entity.dto.*;
import moks.JDBCMok;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.*;


public class TransactionCategoryDaoTest {
    JDBCMok jdbcTest;

    public TransactionCategoryDaoTest() {
    }

    @Before
    public void setUp() {
        jdbcTest = new JDBCMok();
    }

    @Test
    public void findByIDs() {
        
        int transactionId = 2;
        int categoryId = 1;
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM transaction_category_tbl WHERE transaction_id = " + "'" + transactionId + "'"
                            + "and category_id = " + "'" + categoryId + "'");
            ResultSet resultSet = preparedStatement.executeQuery();
            Assert.assertTrue(resultSet.next());
            
        } catch (SQLException | ClassNotFoundException e) {

                e.printStackTrace();

            }

    }

    @Test
    public void findAll() {
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM transaction_category_tbl");
            ResultSet resultSet = preparedStatement.executeQuery();
            Assert.assertTrue(resultSet.next());
            
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
    }

    @Test
    public void removeIds() {

        int transactionId = 4;
        int categoryId = 2;
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement2 = connection.prepareStatement
                    ("INSERT INTO transaction_category_tbl (transaction_id, category_id)\n" +
                            "VALUES (2, 4);", Statement.RETURN_GENERATED_KEYS);
            preparedStatement2.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM transaction_category_tbl WHERE transaction_id = " + "'" + transactionId + "'"
                            + "and category_id = " + "'" + categoryId + "'");
            preparedStatement.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement1 = connection.prepareStatement
                    ("SELECT * FROM transaction_category_tbl WHERE transaction_id = " + "'" + transactionId + "'"
                            + "and category_id = " + "'" + categoryId + "'");
            ResultSet resultSet = preparedStatement1.executeQuery();
            Assert.assertFalse(resultSet.next());
            
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();
        }
    }

    @Test
    public void save() {

        int transactionId = 4;
        int categoryId = 2;
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO transaction_category_tbl (transaction_id, category_id)\n" +
                            "VALUES (2, 4);", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement1 = connection.prepareStatement
                    ("SELECT * FROM transaction_category_tbl WHERE transaction_id = " + "'" + transactionId + "'"
                            + "and category_id = " + "'" + categoryId + "'");
            ResultSet resultSet = preparedStatement1.executeQuery();
            Assert.assertTrue(resultSet.next());
            
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
    }

    @Test
    public void update() {

        int id = 4;
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatementI = connection.prepareStatement
                    ("INSERT INTO transaction_category_tbl (transaction_id, category_id)\n" +
                            "VALUES (2, 4);", Statement.RETURN_GENERATED_KEYS);
            preparedStatementI.executeUpdate();
            connection.commit();
            TransactionCategory transactionCategory = new TransactionCategoryDto(
                    new TransactionTypeDto(4, ""),
                    new CategoryModelDto(2, "", new ParentCategoryDto(1, "")));
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE transaction_category_tbl SET transaction_id = " + "'" + transactionCategory.getTransaction().getId() + "'" +
                            ", category_id = " + "'" + transactionCategory.getCategory().getId() + "'" +
                            "WHERE category_id = " + transactionCategory.getCategory().getId() + "'" +
                            " and transaction_id = " + transactionCategory.getTransaction().getId());
            preparedStatement.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement1 = connection.prepareStatement
                    ("SELECT * FROM transaction_category_tbl WHERE transaction_id = " + "'" + transactionCategory.getTransaction().getId() + "'"
                            + "and category_id = " + "'" + transactionCategory.getCategory().getId() + "'");
            TransactionCategory transactionCategoryUpdate = new TransactionCategoryDto();
            ResultSet resultSet = preparedStatement1.executeQuery();
            resultSet.next();
            transactionCategory.setCategory(new CategoryModelDto(resultSet.getInt(2), "", new ParentCategoryDto(1, "")));
            transactionCategory.setTransaction(new TransactionTypeDto(resultSet.getInt(1), ""));
            
            Assert.assertEquals(transactionCategory, transactionCategoryUpdate);
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
    }
}