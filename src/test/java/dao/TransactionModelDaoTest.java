package dao;


import JDBCTest.JDBCTest;
import entity.*;
import entity.dto.*;
import moks.JDBCMok;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.*;

public class TransactionModelDaoTest {
    JDBCMok jdbcTest;
    public TransactionModelDaoTest() {
    }

    @Before
    public void setUp() {
        jdbcTest = new JDBCMok();
    }
    @Test
    public void findByID() {
      
        int id = 2;
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM transactionModel_tbl WHERE id = " + "'" + id + "'");
            ResultSet resultSet = preparedStatement.executeQuery();
            Assert.assertTrue(resultSet.next());
           
        } catch (SQLException | ClassNotFoundException e) {

                e.printStackTrace();


        }
    }

    @Test
    public void findAll() {
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM transactionModel_tbl");
            ResultSet resultSet = preparedStatement.executeQuery();
            Assert.assertTrue(resultSet.next());
           
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
    }

    @Test
    public void remove() {

        int id = 4;
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement2 = connection.prepareStatement
                    ("\n" +
                            "INSERT INTO transactionModel_tbl(id,name, amount, profitOrLoss, type_id, account_id)\n" +
                            "VALUES (4,'Перевод Саньку с соседнего падъезда', '10000', FALSE, 1, 1);", Statement.RETURN_GENERATED_KEYS);
            preparedStatement2.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM transactionModel_tbl WHERE id = " + id);
            preparedStatement.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement1 = connection.prepareStatement
                    ("SELECT * FROM transactionModel_tbl WHERE id = " + "'" + id + "'");
            ResultSet resultSet = preparedStatement1.executeQuery();
            Assert.assertFalse(resultSet.next());
           
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();
        }
    }

    @Test
    public void save() {
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    (  "INSERT INTO transactionModel_tbl(id,name, amount, profitOrLoss, type_id, account_id)\n" +
                            "VALUES (4,'Перевод Саньку с соседнего падъезда', '10000', FALSE, 1, 1);", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement1 = connection.prepareStatement
                    ("SELECT * FROM parent_category_tbl WHERE id = " + "'" + 5 + "'");
            ResultSet resultSet = preparedStatement1.executeQuery();
            Assert.assertTrue(resultSet.next());
           
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
    }

    @Test
    public void update() {

        int id = 4;
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatementI = connection.prepareStatement
                    ("INSERT INTO transactionModel_tbl(id,name, amount, profitOrLoss, type_id, account_id)\n" +
                            "VALUES (4,'Перевод Саньку с соседнего падъезда', '10000', FALSE, 1, 1);", Statement.RETURN_GENERATED_KEYS);
            preparedStatementI.executeUpdate();
            connection.commit();
            TransactionModel transactionModel = new TransactionModelDto(4, "Test1", 000.0, true,
                    new TransactionTypeDto(2,""),
                    new AccountModelDto(1,"",000.0,new CurrencyModelDto(1,"","","")));
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE transactionModel_tbl SET name = " + "'" + transactionModel.getName() + "'" +
                            ", amount = " + "'" + transactionModel.getAmount() + "'" +
                            ", profitOrLoss = " + "'" + transactionModel.getProfitOrLoss() + "'" +
                            ", type_id = " + "'" + transactionModel.getType().getId() + "'" +
                            ", account_id = " + "'" + transactionModel.getAccount().getId() + "'" +
                            "WHERE ID = " + transactionModel.getId());
            preparedStatement.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement1 = connection.prepareStatement
                    ("SELECT * FROM transactionModel_tbl WHERE id = " + "'" + transactionModel.getId() + "'");
            TransactionModel transactionModelUpdated = new TransactionModelDto();
            ResultSet resultSet = preparedStatement1.executeQuery();
            resultSet.next();
            transactionModelUpdated.setId(resultSet.getInt(1));
            transactionModelUpdated.setName(resultSet.getString(2));
            transactionModelUpdated.setAmount(resultSet.getDouble(3));
            transactionModelUpdated.setProfitOrLoss(resultSet.getBoolean(4));
            transactionModelUpdated.setType(new TransactionTypeDto(resultSet.getInt(5),""));
            transactionModelUpdated.setAccount(new AccountModelDto(resultSet.getInt(6),"",0.00,new CurrencyModelDto(1,"","","")));
           
            Assert.assertEquals(transactionModel, transactionModelUpdated);
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
    }
}