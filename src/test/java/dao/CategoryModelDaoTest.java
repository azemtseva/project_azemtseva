package dao;
import JDBCTest.JDBCTest;
import entity.CategoryModel;
import entity.dto.CategoryModelDto;
import entity.dto.ParentCategoryDto;
import moks.JDBCMok;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.*;

public class CategoryModelDaoTest {
    JDBCMok jdbcTest;

    public CategoryModelDaoTest() {
    }
    @Before
    public void setUp() {
        jdbcTest = new JDBCMok();
    }

    @Test
    public void findByID() {
        
        int id = 2;
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM categoryModel_tbl WHERE id = " + "'" + id + "'");
            ResultSet resultSet = preparedStatement.executeQuery();
            Assert.assertTrue(resultSet.next());
            
        } catch (SQLException | ClassNotFoundException e) {

                e.printStackTrace();

            }

    }

    @Test
    public void findAll() {
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM categoryModel_tbl");
            ResultSet resultSet = preparedStatement.executeQuery();
            Assert.assertTrue(resultSet.next());
            
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
    }

    @Test
    public void remove() {

        int id = 5;
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement2 = connection.prepareStatement
                    ("INSERT INTO categoryModel_tbl (id,name, parent_id)\n" +
                            "VALUES (5,'Test', 1);", Statement.RETURN_GENERATED_KEYS);
            preparedStatement2.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM categoryModel_tbl WHERE id = " + id);
            preparedStatement.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement1 = connection.prepareStatement
                    ("SELECT * FROM categoryModel_tbl WHERE id = " + "'" + id + "'");
            ResultSet resultSet = preparedStatement1.executeQuery();
            Assert.assertFalse(resultSet.next());
            
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();
        }
    }

    @Test
    public void save() {
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO categoryModel_tbl (id,name, parent_id)\n" +
                            "VALUES (5,'Test', 1);", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement1 = connection.prepareStatement
                    ("SELECT * FROM categoryModel_tbl WHERE id = " + "'" + 5 + "'");
            ResultSet resultSet = preparedStatement1.executeQuery();
            Assert.assertTrue(resultSet.next());
            
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
    }

    @Test
    public void update() {

        int id = 5;
        try (Connection connection = jdbcTest.connectionDB()){
            PreparedStatement preparedStatementI = connection.prepareStatement
                    ("INSERT INTO categoryModel_tbl (id,name, parent_id)\n" +
                            "VALUES (5,'Test', 1);", Statement.RETURN_GENERATED_KEYS);
            preparedStatementI.executeUpdate();
            connection.commit();
            CategoryModel categoryModel = new CategoryModelDto(5, "Тест1", new ParentCategoryDto(1,""));
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE categoryModel_tbl SET name = " + "'" + categoryModel.getName() + "'" +
                            ", parent_id = " + "'" + categoryModel.getParent().getId() + "'"  +
                            "WHERE ID = " + categoryModel.getId());
            preparedStatement.executeUpdate();
            connection.commit();
            PreparedStatement preparedStatement1 = connection.prepareStatement
                    ("SELECT * FROM categoryModel_tbl WHERE id = " + "'" + categoryModel.getId() + "'");
            CategoryModel categoryModelUpdate = new CategoryModelDto();
            ResultSet resultSet = preparedStatement1.executeQuery();
            resultSet.next();
            categoryModelUpdate.setId(resultSet.getInt(1));
            categoryModelUpdate.setName(resultSet.getString(2));
            categoryModelUpdate.setParent(new ParentCategoryDto(resultSet.getInt(3),""));
            
            Assert.assertEquals(categoryModel, categoryModelUpdate);
        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
    }
}